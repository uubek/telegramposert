<?php

set_time_limit(10);

require( __DIR__ . '/func.php' );

$channel_id = $_GET['check'];

$res = sm( 'getchatmember', ['chat_id' => $channel_id, 'user_id' => $bot_id] );
$res = json_decode($res);
$can_post = $res->result->can_post_messages;
$can_delete = $res->result->can_delete_messages;
if ( $res->result->status == 'administrator' ) echo "{ok: true, can: {'can_post': $can_post, 'can_delete': $can_delete}}";
else echo "{ok: false}";