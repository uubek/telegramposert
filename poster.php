<?php

set_time_limit(10);

require( __DIR__ . '/func.php' );

date_default_timezone_set('Asia/Tashkent');
$date = date('Y-m-d H:i:s', time());
$date_five = date('Y-m-d H:i:s', time() - 60);
m_query( "set names utf8mb4;" );

if ( $result = m_query( "select * from tasks where message_id = '0' and create_at BETWEEN '$date_five' and '$date';" ) ){
	while ($row = $result->fetch_assoc()) {
		var_dump($row);
		$id = $row['id'];
		$post_id = $row['post_id'];
		$create_at = $row['create_at'];
		$delete_at = $row['delete_at'];
		$top = $row['top'];
		$channel_id = getinfo( 'channels', 'channel_id', 'id', $row['channel_id'] );
		$message_id = $row['message_id'];
		$buttons = getinfo( 'posts', 'buttons', 'id', $post_id );
		$image = getinfo( 'posts', 'image', 'id', $post_id );
		$body = getinfo( 'posts', 'body', 'id', $post_id );
		$body = str_ireplace('<p>', '', $body);
		$body = str_ireplace('</p>', '', $body);
		$body = str_ireplace('&nbsp;', " ", $body);
		$body = str_ireplace('<br>', "\r\n", $body);
		$request = [ 'chat_id' => $channel_id ];
		if ( $image ){
			$method = 'sendphoto';
			$request['photo'] =  $image;
			$request['caption'] = $body;
		}
		else {
			$method = 'sendmessage';
			$request['text'] = $body;
		}
		$request['parse_mode'] = 'HTML';
		if ( !empty($buttons) ) $request['reply_markup'] = $buttons;
		print_r($buttons);
		$res = sm( $method, $request );
		$res = json_decode($res);
		$post_message_id = $res->result->message_id;
		sm( 'forwardMessage', [ 'chat_id' => '-1001225346160', 'from_chat_id' => $channel_id, 'message_id' => $post_message_id, 'disable_notification' => 1 ] );
		m_query( "update tasks set message_id = '$post_message_id', status = '1' where id = '$id'" );
	}
	$result->free();
}

if ( $result = m_query( "select * from tasks where message_id = '0' and delete_at BETWEEN '$date_five' and '$date';" ) ){
	while ($row = $result->fetch_assoc()) {
		$id = $row['id'];
		$post_id = $row['post_id'];
		$channel_id = getinfo( 'channels', 'channel_id', 'id', $row['channel_id'] );
		$message_id = $row['message_id'];
		$is_delete = getinfo( 'post_deletes', 'id', 'task_id', $id );
		if ( empty($is_delete) ){
			$res = sm ( 'deletemessage', [ 'chat_id' => $channel_id, 'message_id' => $message_id ] );
		}
		$res = json_decode($res);
		if ( $res->ok == 'true' ){
			m_query( "insert into post_deletes ( `task_id`, `post_id` ) values ( '$id', '$post_id' )" );
			$status = 2;
		}
		if ($res->ok != 'true') $status = 3;
		m_query( "update tasks set status = '$status' where id = '$id'" );
	}
	$result->free();
}